### Classes[6] = "A Primer on Biological Seqs"

##### Perspetivas em Bioinformática 2023-2024

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

<p><img style="vertical-align:middle" src="../presentation_assets/mastodon-icon.svg" alt="Mastodon icon" width="40px", height="40px"> <a href="https://scholar.social/@FPinaMartins">@FPinaMartins@scholar.social</a></p>

---

### Summary

* Sequence representation reminder
* "Low throughput" data
  * FASTA format
  * Other alignment formats
  * Obtaining data
* "High throughput" data
  * FASTQ format
  * SAM/BAM
* Practical examples

---

### Short reminder

![IUPAC](assets/IUPAC.png)

IUPAC codes

|||

## Nucleotides

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=2 WIDTH="600" style="font-size:50%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC nucleotide code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Base</font></td>
</tr>
<tr>
<td>A</td>
<td>Adenine</td>
</tr>
<tr>
<td>C</td>
<td>Cytosine</td>
</tr>
<tr>
<td>G</td>
<td>Guanine</td>
</tr>
<tr>
<td>T (or U)</td>
<td>Thymine (or Uracil)</td>
</tr>
<tr>
<td>R</td>
<td>A or G</td>
</tr>
<tr>
<td>Y</td>
<td>C or T</td>
</tr>
<tr>
<td>S</td>
<td>G or C</td>
</tr>
<tr>
<td>W</td>
<td>A or T</td>
</tr>
<tr>
<td>K</td>
<td>G or T</td>
</tr>
<tr>
<td>M</td>
<td>A or C</td>
</tr>
<tr>
<td>B</td>
<td>C or G or T</td>
</tr>
<tr>
<td>D</td>
<td>A or G or T</td>
</tr>
<tr>
<td>H</td>
<td>A or C or T</td>
</tr>
<tr>
<td>V</td>
<td>A or C or G</td>
</tr>
<tr>
<td>N</td>
<td>any base</td>
</tr>
<tr>
<td>-</td>
<td>gap</td>
</tr>
</table>

---

### Getting data

![DNA lab](assets/DNA_lab.jpg)

---

### Sanger sequencing

* [How Sanger sequencing works](https://www.youtube.com/watch?v=KTstRrDTmWI)

</br>

<img src="assets/chroma.png" style="background:none; border:none; box-shadow:none;" class="fragment">

|||

### Sanger sequencing

* &shy;<!-- .element: class="fragment" -->Golden Standard
* &shy;<!-- .element: class="fragment" -->High quality
* &shy;<!-- .element: class="fragment" -->Low throughput
* &shy;<!-- .element: class="fragment" -->Large files
  * &shy;<!-- .element: class="fragment" -->[Trace files](https://gitlab.com/bioinformatics-classes/bio-seqs/raw/master/assets/New_traces.zip)
  * &shy;<!-- .element: class="fragment" -->How do we read these?
    * &shy;<!-- .element: class="fragment" -->[CutePeaks](https://github.com/labsquare/CutePeaks)
    * &shy;<!-- .element: class="fragment" -->[SeqTrace](https://github.com/stuckyb/seqtrace)

---

### "Low Throughput" Sequence formats

* &shy;<!-- .element: class="fragment" -->Some of the most frequent file formats
  * &shy;<!-- .element: class="fragment" -->[FASTA](https://zhanglab.ccmb.med.umich.edu/FASTA/)
  * &shy;<!-- .element: class="fragment" -->[GB](https://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html)
  * &shy;<!-- .element: class="fragment" -->[MEGA](https://www.megasoftware.net/webhelp/walk_through_mega/mega_basics_hc.htm)
  * &shy;<!-- .element: class="fragment" -->[ALN](http://meme-suite.org/doc/clustalw-format.html)
  * &shy;<!-- .element: class="fragment" -->[NEXUS](https://plewis.github.io/nexus/)
  * &shy;<!-- .element: class="fragment" -->[PHYLIP](http://www.phylo.org/tools/obsolete/phylip.html)
  * &shy;<!-- .element: class="fragment" -->...

---

### DNA sequence databases

* &shy;<!-- .element: class="fragment" -->[NCBI (USA)](https://www.ncbi.nlm.nih.gov/)
* &shy;<!-- .element: class="fragment" -->[ENA (Europe)](https://www.ebi.ac.uk/ena/browser/home)
* &shy;<!-- .element: class="fragment" -->[DDBJ (Japan)](https://www.ddbj.nig.ac.jp)
  * &shy;<!-- .element: class="fragment" -->Data repositories
  * &shy;<!-- .element: class="fragment" -->Replicated
  * &shy;<!-- .element: class="fragment" -->Queryable

<img class="fragment" src="assets/data_center.jpg" style="background:none; border:none; box-shadow:none;">

---

### Obtaining sequences

<img class="fragment" src="assets/psammodromus.jpg" style="background:none; border:none; box-shadow:none;">

* &shy;<!-- .element: class="fragment" -->Meet *Psammodromus algirus*
  * &shy;<!-- .element: class="fragment" -->Go to the [NCBI](https://www.ncbi.nlm.nih.gov/) website
  * &shy;<!-- .element: class="fragment" -->Select the "nucleotide" database
  * &shy;<!-- .element: class="fragment" -->Search for *Psammodromus algirus[organism], cytb[gene]*
* &shy;<!-- .element: class="fragment" -->Click "Send to" and get a file with all the sequences in FASTA format
  * &shy;<!-- .element: class="fragment" -->Save it as `~/sequences/P_algirus_NCBI.fasta`

|||

### Obtaining sequences

<img class="fragment" src="assets/psammodromus.jpg" style="background:none; border:none; box-shadow:none;">

* &shy;<!-- .element: class="fragment" -->Now try the same on [ENA](https://www.ebi.ac.uk/ena/browser/home)
  * &shy;<!-- .element: class="fragment" -->Press the "search" button
  * &shy;<!-- .element: class="fragment" -->Search for *Psammodromus algirus, cytb*
  * &shy;<!-- .element: class="fragment" -->Select the "Sequence" field
* &shy;<!-- .element: class="fragment" -->Get a file with all the sequences in FASTA format
  * &shy;<!-- .element: class="fragment" -->Click "Download ENA records: FASTA"
  * &shy;<!-- .element: class="fragment" -->Save it as `~/sequences/P_algirus_ENA.fasta`

---

### FASTA files

```bash
cd ~/sequences
ls

# Quick look
nano P_algirus_NCBI.fasta
nano P_algirus_ENA.fasta

# Get sequence names
grep "^>" P_algirus_NCBI.fasta
grep "^>" P_algirus_ENA.fasta

# Count number of sequences
grep -c "^>" P_algirus_NCBI.fasta
grep -c "^>" P_algirus_ENA.fasta

```

* Does this look familiar? <!-- .element: class="fragment" data-fragment-index="1" -->

|||

### Bonus:

* Are these files different?
* How?
* Can you show these differences?

```bash
# SPOILER ALERT!
# Scroll below for the solution

















# Let's gather all the names:
grep ">" P_algirus_ENA.fasta > ENA_names.txt
grep ">" P_algirus_NCBI.fasta > NCBI_names.txt

# Something is still not right - look at the accession numbers and ".$" !
grep "^>" P_algirus_ENA.fasta| cut -d "|" -f 3 |sed 's/\.$//' > ENA_names.txt
grep "^>" P_algirus_NCBI.fasta| tr -d ">" > NCBI_names.txt

# Now they look the same, but they are not in the same order!
sort ENA_names.txt > ENA_names_sorted.txt
sort NCBI_names.txt > NCBI_names_sorted.txt

# Alternatively:
grep "^>" P_algirus_ENA.fasta| cut -d "|" -f 3 |sed 's/\.$//' |sort > ENA_names_sorted.txt
grep "^>" P_algirus_NCBI.fasta| tr -d ">" |sort > NCBI_names_sorted.txt

# What? That's a lot of lines to look for manually!!
diff ENA_names_sorted.txt NCBI_names_sorted.txt
## or
diff NCBI_names_sorted.txt ENA_names_sorted.txt
```

---

### The other way around

* &shy;<!-- .element: class="fragment" -->It is also possible to provide the database with a sequence and match it to its entries
* &shy;<!-- .element: class="fragment" -->NCBI [BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi)
  * &shy;<!-- .element: class="fragment" -->Try it with [This sequence](https://gitlab.com/bioinformatics-classes/bio-seqs/-/raw/master/assets/sequence_01.fasta)
  * &shy;<!-- .element: class="fragment" -->Can you tell which organism and gene this sequence represents?
* &shy;<!-- .element: class="fragment" -->[Ok, so how large it this thing?](https://www.ncbi.nlm.nih.gov/genbank/statistics/)

---

### Storage vs alignment

* &shy;<!-- .element: class="fragment" -->FASTA files are good for storing data
* &shy;<!-- .element: class="fragment" -->Making the data comparable is something else
* &shy;<!-- .element: class="fragment" -->For that, we need **alignments**
  * &shy;<!-- .element: class="fragment" -->Each base in our sequence needs to be aligned so that their positions are comparable
* &shy;<!-- .element: class="fragment" -->Some formats are designed for aligned data

<img class="fragment" src="assets/unalign_vs_align.png" style="background:none; border:none; box-shadow:none;">

|||

### Aligning sequence data

```bash
# Create a directory to perform our tests
mkdir alignments
cd alignments

# Obtain a sequence alignment viewer and use it
wget https://github.com/mpdunne/alan/archive/2.1.1.tar.gz
tar xvfz 2.1.1.tar.gz
cd alan-2.1.1
./alan ~/sequences/P_algirus_NCBI.fasta

# Align the dataset
mafft ~/sequences/P_algirus_NCBI.fasta > ~/alignments/P_algirus_aligned.fasta
# Wait, what?

# Look at it again
./alan ~/alignments/P_algirus_aligned.fasta


```

---

### There must be a better way!

[![AliView](assets/aliview.png)](https://ormbunkar.se/aliview/)

* &shy;<!-- .element: class="fragment" -->Install [AliView](https://ormbunkar.se/aliview/) on your OS
  * &shy;<!-- .element: class="fragment" -->Aliview is not packaged in `conda` nor `apt` so you have to get your hand dirty and manually install it
  * &shy;<!-- .element: class="fragment" -->Follow the on-site instructions
* &shy;<!-- .element: class="fragment" -->You will need to install Java, **FIRST** though!

<div class='fragment'>

```bash
sudo apt update
sudo apt install openjdk-8-jre
```

</div>

---

### "Low Throughput" Sequence formats

* &shy;<!-- .element: class="fragment" -->[Sequence storage files](assets/unaligned_seqs.tar.xz)
* &shy;<!-- .element: class="fragment" -->[Sequence alignment files](assets/aligned_seqs.tar.xz)
  * &shy;<!-- .element: class="fragment" -->Download these files to your OS, uncompress them and take a look at their contents
  * &shy;<!-- .element: class="fragment" -->They all contain (mostly) the same information, but in different formats
  * &shy;<!-- .element: class="fragment" -->Find the differences!


|||

### Phylip

```
9 50
TXVZ-2A      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGCATCGGATAA 
TXM1L        AGATTTCTGCATATACGTCCAAATCGATCAATAGTATCCGAATCGGATAA 
TXM10        AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
TXV-2V       AGATTTCTGCATATACGTCCATATCGATCAATAATATCCGAATCGGATAA 
TXVZ-4A      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
TXVZ-3A      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCCGATAA 
TXNoGAP      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
TW24         AGA---CTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
TW3L         AGATTTCTGCACATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
```

|||

### Nexus

```
#NEXUS
[File generated by DnaSP Ver. 5.10.01, from file: teaching.fasta     Aug 19, 2013]

BEGIN TAXA;
DIMENSIONS NTAX=9;
TAXLABELS
'TXVZ-2A'
'TXM1L'
'TXM10'  
'TXV-2V' 
'TXVZ-4A'
'TXVZ-3A'
'TXNoGAP'
'TW24'  
'TW3L';
END;

BEGIN CHARACTERS;
DIMENSIONS NCHAR=16;
FORMAT DATATYPE=DNA  MISSING=N GAP=-;
MATRIX

'TXVZ-2A'      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGCATCGGATAA 
'TXM1L'        AGATTTCTGCATATACGTCCAAATCGATCAATAGTATCCGAATCGGATAA 
'TXM10'        AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
'TXV-2V'       AGATTTCTGCATATACGTCCATATCGATCAATAATATCCGAATCGGATAA 
'TXVZ-4A'      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
'TXVZ-3A'      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCCGATAA 
'TXNoGAP'      AGATTTCTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
'TW24'         AGA---CTGCATATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
'TW3L'         AGATTTCTGCACATACGTCCAAATCGATCAATAATATCCGAATCGGATAA 
;
END;

BEGIN PROGRAMNAME;
    Setting1=Whatever;
    Setting2=Something;
    Setting3=YadaYada;
END;
```

---

### High throughput sequencing

* &shy;<!-- .element: class="fragment" -->What if we need to scale things?
  * &shy;<!-- .element: class="fragment" -->(And not spend our entire budget on sequencing)
* &shy;<!-- .element: class="fragment" -->Unknown genome regions
  * &shy;<!-- .element: class="fragment" -->Non-model organisms
* &shy;<!-- .element: class="fragment" -->We need more power

<a href="https://what-if.xkcd.com/13/"><img class="fragment" src="assets/laser_pointer_more_power.png" style="background:none; border:none; box-shadow:none;"></a>

|||

### Enter the [FASTQ](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/FileFormat_FASTQ-files_swBS.htm) [format](https://pythonhosted.org/OBITools/fastq.html)

* &shy;<!-- .element: class="fragment" -->Each chromatogram takes 100 ~ 200 KB
  * &shy;<!-- .element: class="fragment" -->200M reads * 150KB = 30TB
  * &shy;<!-- .element: class="fragment" -->Chromatograms just don't scale!
* &shy;<!-- .element: class="fragment" -->Each FASTQ sequence is composed of 4 lines:
  * &shy;<!-- .element: class="fragment" -->`@Sequence_identifier`
  * &shy;<!-- .element: class="fragment" -->`ATGCGATAGCTGACTGACTAGCT`
  * &shy;<!-- .element: class="fragment" -->`+` (optionally the seq_id again)
  * &shy;<!-- .element: class="fragment" -->`!''*(((((******+**,-`

<img class="fragment" src="assets/Probabilitymetrics.png" style="background:none; border:none; box-shadow:none;"></a>

|||

### FASTQ format

```bash
# Obtian a fastq sequence file
cd ~/
mkdir hts_data
cd hts_data
wget https://gitlab.com/bioinformatics-classes/bio-seqs/raw/master/assets/short_reads.fastq

# Look at the file contents
nano short_reads.fastq

# How much space does this file occupy?
# How many sequences are represented?
```

---

### Assmblies

* &shy;<!-- .element: class="fragment" -->Sequence assemblies are a huge problem
  * &shy;<!-- .element: class="fragment" -->HTS reads come from random genome locations
  * &shy;<!-- .element: class="fragment" -->We could use an entire semester to deal with this problem
* &shy;<!-- .element: class="fragment" -->Two types of sequence assembly
  * &shy;<!-- .element: class="fragment" -->*Mapping* assemblies (reference available)
  * &shy;<!-- .element: class="fragment" -->*Denovo* assemblies (reference unavailable)

|||

### Assemblies

<img src="assets/NGS-image.png" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="assets/Puzzle_and_box.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="assets/Puzzle_no_box.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="assets/krypt-puzzle-image.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="assets/EST_puzzle.jpg" style="background:none; border:none; box-shadow:none;"></a>

|||

### Assemblies

<img src="assets/mismatch.jpg" style="background:none; border:none; box-shadow:none;"></a>

---

### [SAM/BAM format](https://samtools.github.io/hts-specs/SAMv1.pdf)

* &shy;<!-- .element: class="fragment" -->The "standard" way to represent assembled data
  * &shy;<!-- .element: class="fragment" -->Contain the reads and their coordinates relative to a reference / each other
  * &shy;<!-- .element: class="fragment" -->A BAM file is a binary version of a SAM file
  * &shy;<!-- .element: class="fragment" -->BAM files can be indexed

---

### In practice

```bash
# Create a new conda environment:
conda create -n mapping
conda activate mapping  # Notice the prompt change!

# Install the required tools:
conda install bowtie2 samtools tablet

# Get a reference sequence
cd ~/hts_data
wget https://gitlab.com/bioinformatics-classes/bio-seqs/raw/master/assets/reference.fasta

# Look at the obtained file. What is this about?
less reference.fasta

# Now, let the magic begin.
# First we need to create an index from our reference sequence and look at what happened
bowtie2-build reference.fasta lambda-ref
ls

# Next, we align our previously downloaded reads to our reference and look at the resulting file
bowtie2 -x lambda-ref -U short_reads.fastq -S assembly.sam
nano assembly.sam

# Then we convert the assembly to a sorted binary file (BAM) using samtools
samtools view -bS assembly.sam | samtools sort - -o assembly.bam

# If all goes well, this command should not show any output in the console
# Use `ls` to make sure a new `assembly.bam` file was created
ls

# Finally we index our BAM file.
# Use `ls -l` to look at the file sizes (specifically, compare the `.sam` and `.bam` files)
samtools index assembly.bam
ls -l

# Now we can look at the final result using *tablet*
# "Open Assembly" -> Select the BAM file as the assembly and the fasta file as the reference
tablet
```

[A more detailed tutorial](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#getting-started-with-bowtie-2-lambda-phage-example)

---

### Final challange

* Consider the data you have just mapped (`assembly.bam`)
* Use `samtools` (and any other shell tools you find relevant) to output the number of reads (coverage) in position number "XXXX", where "XXXX" are your student number's **last** 4 digits;
  * The output of your command should be the depth value and **nothing else**!
* Write a (non-interactive) script that reports the coverage of **any** user provided position ([pass the position as an argument](https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script), or read it from STDIN);
* Use shell tools to run the script and have it write the results to a text file

---

### References

* [IUPAC reference](https://www.bioinformatics.org/sms/iupac.html)
* [How Sanger sequencing works](https://cnx.org/contents/GFy_h8cu@10.4:U7tPDRxK@7/DNA-Structure-and-Sequencing)
* [NCBI search terms list](https://www.ncbi.nlm.nih.gov/books/NBK49540/)
* [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml)
* [Assemblathon 2 (internet archive link)](https://web.archive.org/web/20221222212231/https://assemblathon.org/assemblathon2)
